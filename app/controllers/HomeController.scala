package controllers

import javax.inject._

import play.api._
import play.api.mvc._
import utils.VideoService

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(cc: ControllerComponents,
                               configuration: Configuration,
                               ps: VideoService) extends AbstractController(cc) {
    
    import scala.concurrent.ExecutionContext.Implicits.global

  /**
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
    def index = Action { implicit request: Request[AnyContent] =>
        Ok(views.html.index(ps.uploaded))
    }
    
    def upload = Action(parse.multipartFormData) { request =>
        request.body.files.foreach(ps.upload)
        Redirect(routes.HomeController.index(), 200)
    }
    
    def preview(id: String) = Action { _ =>
        ps.preview(id) match {
            case Some(file) => Ok.sendFile(file)
            case _ => NotFound
        }
    }
    
    def download(id: String) = Action { _ =>
        ps.content(id) match {
            case Some(file) => Ok.sendFile(file)
            case _ => NotFound
        }
    }
}
