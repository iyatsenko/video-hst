package utils

import java.io.File
import java.nio.file.Paths
import java.util.UUID
import javax.inject.{Inject, Singleton}

import com.google.common.io.PatternFilenameFilter
import play.api.{Configuration, Logger}
import play.api.libs.Files
import play.api.mvc.MultipartFormData

import scala.util.Try

@Singleton
class VideoService @Inject()(configuration: Configuration) {
    
    val mediaRoot: String = configuration.get[String]("media.root")
    val allowedExtensions: Seq[String] =
        configuration.getOptional[Seq[String]]("media.extensions").getOrElse(Seq.empty).map(_.toLowerCase)
    
    val systemCode: Option[String] = configuration.getOptional[String]("os_code").map(_.toLowerCase)
    
    def id2path(id: String): String = Paths.get(mediaRoot, id).toString
    
    private var _uploaded = {
        val dir = new File(mediaRoot)
        
        if(dir.exists)
            if(dir.isDirectory) dir.listFiles.map(_.getName).filterNot(_.startsWith(".")).toSeq
            else {
                dir.mkdir
                Seq.empty[String]
            }
        else {
            dir.mkdir
            Seq.empty[String]
        }
    }
    def uploaded = _uploaded
    def folders: Seq[String] = _uploaded.map(id2path)
    
    private def update(id: String): Unit = content(id).map(_.getAbsolutePath)match {
            case Some(in) =>
            
                val out = s"${id2path(id)}/preview.gif"
                systemCode match {
                    case Some("linux") => sys.process.Process(s"./ffmpeg -t 3 -i $in -vf scale=320:240 $out")!
                    case Some("mac") => sys.process.Process(s"./ffmpeg-mac -t 3 -i $in -vf scale=320:240 $out")!
                    case Some("win") => sys.process.Process(s"./ffmpeg.exe -t 3 -i $in -vf scale=320:240 $out")!
                    case Some(unknown) =>
                        Logger.warn(s"Detected $unknown system! Trying start ffmpeg as linux version!")
                        sys.process.Process(s"./ffmpeg -t 3 -i $in -vf scale=320:240 $out")!
                    case _ => Logger.error(s"Current system's code not specified!")
                }
        
            case _ =>
        }
    
    def upload(file: MultipartFormData.FilePart[Files.TemporaryFile]): Unit = {
        val fileExtension = file.filename.reverse.takeWhile(_.isLetterOrDigit).reverse.toLowerCase
        if (allowedExtensions.contains(fileExtension)) {
            val id = UUID.randomUUID.toString
            val dirPath = Paths.get(mediaRoot, id).toString
            new File(dirPath).mkdir()
            file.ref.moveTo(Paths.get(dirPath, s"content.$fileExtension"), replace = true)
            update(id)
            _uploaded = _uploaded :+ id
        }
    }
    def preview(id: String): Option[File] = Try(new File(Paths.get(id2path(id), "preview.gif").toString)).toOption
    def content(id: String): Option[File] = {
        val dir = new File(id2path(id))
        if(!dir.exists) None
        if(dir.isDirectory) dir.listFiles(new PatternFilenameFilter("content.*")).headOption
        else None
    }
    
    def remove(id: String): Boolean = {
        val path = id2path(id)
        val file = new File(path)
        if(file.exists) file.delete()
        else false
    }
}
