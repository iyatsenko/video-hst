Scala version 2.12.3
SBT version 1.0.3

Для первого запуска необходимо иметь подключение к интернету (для загрузки сторонних библиотек: guice, play-компоненты). Форматы видео-файлов, загружаемых в приложение прописываются в ./config/application.conf/media.extensions

How to:
	0. Выбрать из предложенных вариантов операционную систему в conf/application.conf/os_code (default = linux)
	1. "$ cd video-hst/"
	2. "$ sbt run"
	3. http://localhost:<port> (port - по-умолчанию 9000)
